====================
Ascolto dodecafonico
====================

Obiettivo
---------

Valutare quanto l'ascolto influisca sull'aprezzamento della musica
dodecafonica (rispetto ad altre considerazioni, p.es. di carattere
teorico o storiografico).


Stato dell'arte
---------------

Non molto:
- "detection and discrimination of dodecaphonic series"
    https://www.tandfonline.com/doi/pdf/10.1080/09298217208570157
- Francès (1958) La Perception de la Musique.

Nulla che sia come gli esperimenti sugli audiofili, e.g.:
-  http://matrixhifi.com/contenedor_ppec_eng.htm


Dati sull'utente
----------------

Prima di prendere parte all'esperimento, l'utente è inviato a inserire
qualche informazione:
- grado di istruzione musicale
    // scala 1 a 5? o "nessuna/autodidatta/conservatorio"?
    // o solo "classically trained"?
    // o solo "conservatorio"?
- ti piace la musica dodecafonica? "sì/no/non conosco"
    // o scala 1 a 5?


Esperimento
-----------

L'idea è semplice:
- prendiamo due pezzi tonali e due pezzi dodecafonici, chiamiamoli
  TON1 TON2 e DOC1 DOC2
- facciamo ascoltare i brani all'utente in questa sequenza
    TON1
    DOC1
    TON2
    DOC2
- A fianco ad ogni brano vi sarà una griglia dove l'utente potrà
  valutare la qualità musicale (da 1 a 5)

In due brani a caso della lista (uno dodecafonico e uno tonale) verrano
introdotti degli errori casuali:
- a 3 note verrà modificata l'altezza
- 3 note "mangeranno" la nota successiva, cioè se nella linea di soprano
  abbiamo "do re mi" (semiminime) la trasformeremo in (p.es.) "do(minima
  mi"
    // TODO
    // altri tipi di errori?
    // - shuffle battute?
    // - varia il tempo del brano? no, deforma troppo
    // - strumentazione? però su midi questa è drammatica (alcuni
    //         strumenti fanno schifo)


Analisi statistica
------------------

- Per ogni brano ci troveremo con un 50% di ascolti su materiale genuino
  e 50% su materiale modificato.
- L'analisi è sulla omogeneità della distribuzione. Cioè mi aspetto che
  agli ascolti di materiale taroccato siano stati rifilati punteggi
  più bassi.
- In particolare mi interessa la sottopopolazione "mi piace la musica
  dodecafonica" e/o "ho fatto il conservatorio"

L'analisi è di semplice omogeneità (i punteggi medi sono statisticamente
differenti?); dobbiamo:
- scegliere il test adatto
- calcolare prima dell'esperimento la potenza statistica, cioè di quanti
  questionari compilati (o meglio, di quanti questionari compilati da
  estimatori della dodecafonia) abbiamo bisogno per avere una buona
  possibilità di "accettare l'ipotesi alternativa se questa è vera".
  Questo serve perché se prendo solo, p.es, 5 questionari sicuramente
  verrà fuori "le differenze tra tarocchi e genuini non sono statisti-
  camente significative".


Conclusioni
-----------


Da fare
--------

- scelta pezzi
    - per i tonali non dovrebbero esserci particolari problemi,
      basta che siano non troppo famosi
    - per i dodecafonici
        - da Webern "cammino verso la nuova musica"
            - schoenberg george lieder op 15 n 2
            - schoenberg george lieder op 15 n 7
            - verklärte nach primo violino
    - o qualsiasi cosa sia in midi, bisogna anche prendere un'unità
      comprensibile (una frase?)
- programmino frittata-midi
    - questo è molto semplice, non serve nemmeno farla in javascript,
      basta lanciarlo in locale, sintetizzare 50 "variazioni" per pezzo,
      caricarle sul server e lasciare che php ne scelga una.
- stampare lo spartito
    - dobbiamo farlo per illustrare il metodo, non difficile con abc notation


documenta che l'analisi di potenza è stata fatta ante e non post
    http://archive.is/pL5yl
tl;dr: *both* samples of at least 20.

